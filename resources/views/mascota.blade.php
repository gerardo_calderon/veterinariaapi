{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('nombre', 'Nombre:') !!}
			{!! Form::text('nombre') !!}
		</li>
		<li>
			{!! Form::label('fecha_nacimiento', 'Fecha_nacimiento:') !!}
			{!! Form::text('fecha_nacimiento') !!}
		</li>
		<li>
			{!! Form::label('icono', 'Icono:') !!}
			{!! Form::text('icono') !!}
		</li>
		<li>
			{!! Form::label('idusuario', 'Idusuario:') !!}
			{!! Form::text('idusuario') !!}
		</li>
		<li>
			{!! Form::label('idtipo', 'Idtipo:') !!}
			{!! Form::text('idtipo') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}