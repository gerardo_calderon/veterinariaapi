{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('fecha', 'Fecha:') !!}
			{!! Form::text('fecha') !!}
		</li>
		<li>
			{!! Form::label('fecha_proxima', 'Fecha_proxima:') !!}
			{!! Form::text('fecha_proxima') !!}
		</li>
		<li>
			{!! Form::label('dosis', 'Dosis:') !!}
			{!! Form::text('dosis') !!}
		</li>
		<li>
			{!! Form::label('estado', 'Estado:') !!}
			{!! Form::text('estado') !!}
		</li>
		<li>
			{!! Form::label('idmascota', 'Idmascota:') !!}
			{!! Form::text('idmascota') !!}
		</li>
		<li>
			{!! Form::label('idvacuna', 'Idvacuna:') !!}
			{!! Form::text('idvacuna') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}