{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('fecha', 'Fecha:') !!}
			{!! Form::text('fecha') !!}
		</li>
		<li>
			{!! Form::label('altura', 'Altura:') !!}
			{!! Form::text('altura') !!}
		</li>
		<li>
			{!! Form::label('peso', 'Peso:') !!}
			{!! Form::text('peso') !!}
		</li>
		<li>
			{!! Form::label('idmascota', 'Idmascota:') !!}
			{!! Form::text('idmascota') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}