<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Usuario extends Model {

	protected $table = 'usuario';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

}