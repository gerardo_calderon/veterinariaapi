<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MascotaVacuna extends Model {

	protected $table = 'mascota_vacuna';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	public function mascota()
	{
		return $this->hasOne('App\Mascota', 'id', 'idmascota');
	}

	public function vacuna()
	{
		return $this->hasOne('App\Vacuna', 'id', 'idvacuna');
	}

}