<?php namespace App\Http\Controllers;

class VacunaController extends BaseTokenController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$idMascota = (int)\Input::get('mascota');
		$vacunas = \App\MascotaVacuna::where('idmascota','=',$idMascota)->get();

		$data = array();
		foreach ($vacunas as $vacuna) {
			$data[] = array(
				"idMascota"=>$vacuna->idmascota,
				"Vacuna"=>$vacuna->id, 
				"NombreVacuna"=>$vacuna->vacuna->nombre,
				"fecha"=>$vacuna->fecha, 
				"fechaProxima"=>$vacuna->fecha_proxima,
				"dosis"=>$vacuna->dosis,
				"estado"=>$vacuna->estado,
				"descripcion"=>$vacuna->descripcion
			);
		}
		$response = array( "success"=>"true", "data"=>$data );
		return \Response::json($response);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$vacuna = \App\MascotaVacuna::find($id);
		if($vacuna==null){
			$this->buildErrorMessage('No existe',404);
		}
		if($vacuna->mascota->idusuario != $this->idUser){
			$this->buildErrorMessage('No permitido');
		}

		$data = array(
			"Vacuna"=>$vacuna->id, 
			"idMascota"=>$vacuna->idmascota,
			"NombreVacuna"=>$vacuna->vacuna->nombre,
			"fecha"=>$vacuna->fecha, 
			"fechaProxima"=>$vacuna->fecha_proxima,
			"dosis"=>$vacuna->dosis,
			"estado"=>$vacuna->estado,
			"descripcion"=>$vacuna->descripcion
		);

		$response = array("success"=>"true","vacuna"=>$data);
		return \Response::json($response);
	}

}

?>