<?php namespace App\Http\Controllers;

class BaseTokenController extends Controller {

	protected $idUser = 0;
	protected $user = null;
	
	public function __construct(){
		if(!\Input::has('token'))
			$this->buildErrorMessage("Acceso no autorizado");
		

		$token = \Input::get('token');
		$usuario = \App\Usuario::where('token','=',$token)->first();
		//echo $token;exit();
		if($usuario==null)
			$this->buildErrorMessage("Acceso no válido");

		$this->user = $usuario;
		$this->idUser = $usuario->id;
	}

	public function buildErrorMessage($mensaje,$error=403){
		abort($error,$mensaje);
	}

}

?>