<?php namespace App\Http\Controllers;

class MascotaController extends BaseTokenController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$mascotas = \App\Mascota::where('idusuario','=',$this->idUser)->get();
		$data = array();
		foreach ($mascotas as $mascota) {
			$data[] = array(
				"idMascota"=>$mascota->id,
				"nombreMascota"=>$mascota->nombre, 
				"fechaNac"=>$mascota->fecha_nacimiento, 
				"icono"=>\URL::asset($mascota->icono),
				"Tipo"=>$mascota->tipo->nombre,
				"Usuario"=>$this->user->nombre
			);
		}
		$response = array( "success"=>"true", "data"=>$data );
		return \Response::json($response);
	}
	

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$mascota = \App\Mascota::find($id);
		if($mascota==null){
			$this->buildErrorMessage('No existe',404);
		}
		if($mascota->idusuario != $this->idUser){
			$this->buildErrorMessage('No permitido');
		}

		$data = array(
			"idMascota"=>$mascota->id,
			"nombreMascota"=>$mascota->nombre,
			"fechaNac"=>$mascota->fecha_nacimiento, 
			"icono"=>\URL::asset($mascota->icono),
			"Tipo"=>$mascota->tipo->nombre,
			"Usuario"=>$this->user->nombre
		);

		$response = array("success"=>"true","detalle"=>$data);
		return \Response::json($response);
	}
}

?>