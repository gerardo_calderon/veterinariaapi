<?php namespace App\Http\Controllers;
use Request;


class LoginController extends Controller {

		/**
		* Display a listing of the resource.
		*
		* @return Response
		*/
		public function index(){
				return "Error, only POST method";
		}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$user = Request::input('user');
		$pass = Request::input('password');
		$usuarios = \App\Usuario::
								where('correo','=',$user)->
								where('password','=',$pass)->get();

		$success = "false";
		if(count($usuarios)==1){
				$usuario = $usuarios[0];
				$usuario->token = \Hash::make($usuario->id.strtotime(date('Y-m-d H:i:s')));
				$usuario->save();

				$success="true";
				$data = array("nombreUsuario"=>$usuario->nombre,"token"=>$usuario->token);
		}else{
				$data = array("nombreUsuario"=>"","token"=>"");
		}

		$response = array("success"=>$success,"login"=>$data);
		return \Response::json($response);
	}

}

?>