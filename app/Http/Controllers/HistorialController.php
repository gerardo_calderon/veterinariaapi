<?php namespace App\Http\Controllers;

class HistorialController extends BaseTokenController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$idMascota = (int)\Input::get('mascota');
		$historiales = \App\Historial::where('idmascota','=',$idMascota)->get();

		$data = array();
		foreach ($historiales as $historial) {
			$data[] = array(
				"idHistorial"=>$historial->id,
				"fechaHistorial"=>$historial->fecha,
				"altura"=>$historial->altura, 
				"peso"=>$historial->peso, 
				"Mascota"=>$historial->mascota->nombre
			);
		}
		$response = array( "success"=>"true", "data"=>$data );
		return \Response::json($response);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$historial = \App\Historial::find($id);
		if($historial==null){
			$this->buildErrorMessage('No existe',404);
		}
		if($historial->mascota->idusuario != $this->idUser){
			$this->buildErrorMessage('No permitido');
		}

		$data = array(
			"idHistorial"=>$historial->id,
			"fechaHistorial"=>$historial->fecha,
			"altura"=>$historial->altura, 
			"peso"=>$historial->peso, 
			"Mascota"=>$historial->mascota->nombre
		);

		$response = array("success"=>"true","vacuna"=>$data);
		return \Response::json($response);
	}

}

?>