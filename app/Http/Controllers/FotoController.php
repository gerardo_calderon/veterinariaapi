<?php namespace App\Http\Controllers;

class FotoController extends BaseTokenController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$idMascota = (int)\Input::get('mascota');
		$fotos = \App\Foto::where('idmascota','=',$idMascota)->get();

		$data = array();
		foreach ($fotos as $foto) {
			$data[] = array(
				"idFoto"=>$foto->id,
				"foto"=>\URL::asset($foto->url),
				"Mascota"=>$foto->mascota->nombre
			);
		}
		$response = array( "success"=>"true", "data"=>$data );
		return \Response::json($response);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$foto = \App\Foto::find($id);
		if($foto==null){
			$this->buildErrorMessage('No existe',404);
		}
		if($foto->mascota->idusuario != $this->idUser){
			$this->buildErrorMessage('No permitido');
		}

		$data = array(
			"idFoto"=>$foto->id,
			"foto"=>\URL::asset($foto->url),
			"Mascota"=>$foto->mascota->nombre
		);

		$response = array("success"=>"true","galeria"=>$data);
		return \Response::json($response);
	}

	
}

?>