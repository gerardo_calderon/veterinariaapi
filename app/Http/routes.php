<?php 

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('app');
});


Route::resource('auth', 'LoginController');
Route::resource('mascota', 'MascotaController');
Route::resource('usuario', 'UsuarioController');
Route::resource('foto', 'FotoController');
Route::resource('tipo', 'TipoController');
Route::resource('mascotavacuna', 'MascotaVacunaController');
Route::resource('vacuna', 'VacunaController');
Route::resource('historial', 'HistorialController');
