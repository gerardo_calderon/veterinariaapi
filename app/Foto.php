<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Foto extends Model {

	protected $table = 'foto';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	public function mascota()
	{
		return $this->hasOne('App\Mascota', 'id', 'idmascota');
	}

}