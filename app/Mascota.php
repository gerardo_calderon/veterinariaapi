<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mascota extends Model {

	protected $table = 'mascota';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	public function usuario()
	{
		return $this->hasOne('App\Usuario', 'id', 'idusuario');
	}

	public function foto()
	{
		return $this->hasMany('App\Foto', 'id', 'idmascota');
	}

	public function tipo()
	{
		return $this->hasOne('App\Tipo', 'id','idtipo');
	}

	public function mascotaVacuna()
	{
		return $this->hasMany('App\MascotaVacuna', 'id', 'idmascota');
	}

	public function historial()
	{
		return $this->hasMany('App\Historial', 'id', 'idmascota');
	}

}