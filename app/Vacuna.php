<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vacuna extends Model {

	protected $table = 'vacuna';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	public function mascotaVacuna()
	{
		return $this->hasMany('App\MascotaVacuna', 'id', 'idvacuna');
	}

}