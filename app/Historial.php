<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Historial extends Model {

	protected $table = 'historial';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	public function mascota()
	{
		return $this->hasOne('App\Mascota', 'id', 'idmascota');
	}

}