<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipo extends Model {

	protected $table = 'tipo';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	public function mascota()
	{
		return $this->hasMany('App\Mascota', 'id', 'idmascota');
	}

}