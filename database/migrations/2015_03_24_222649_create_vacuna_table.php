<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVacunaTable extends Migration {

	public function up()
	{
		Schema::create('vacuna', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('nombre', 45);
		});
	}

	public function down()
	{
		Schema::drop('vacuna');
	}
}