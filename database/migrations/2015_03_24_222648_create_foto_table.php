<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFotoTable extends Migration {

	public function up()
	{
		Schema::create('foto', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('url');
			$table->integer('idmascota')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('foto');
	}
}