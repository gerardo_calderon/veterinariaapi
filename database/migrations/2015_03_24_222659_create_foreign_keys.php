<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('mascota', function(Blueprint $table) {
			$table->foreign('idusuario')->references('id')->on('usuario')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('mascota', function(Blueprint $table) {
			$table->foreign('idtipo')->references('id')->on('tipo')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('foto', function(Blueprint $table) {
			$table->foreign('idmascota')->references('id')->on('mascota')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('mascota_vacuna', function(Blueprint $table) {
			$table->foreign('idmascota')->references('id')->on('mascota')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('mascota_vacuna', function(Blueprint $table) {
			$table->foreign('idvacuna')->references('id')->on('vacuna')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('historial', function(Blueprint $table) {
			$table->foreign('idmascota')->references('id')->on('mascota')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('mascota', function(Blueprint $table) {
			$table->dropForeign('mascota_idusuario_foreign');
		});
		Schema::table('mascota', function(Blueprint $table) {
			$table->dropForeign('mascota_idtipo_foreign');
		});
		Schema::table('foto', function(Blueprint $table) {
			$table->dropForeign('foto_idmascota_foreign');
		});
		Schema::table('mascota_vacuna', function(Blueprint $table) {
			$table->dropForeign('mascota_vacuna_idmascota_foreign');
		});
		Schema::table('mascota_vacuna', function(Blueprint $table) {
			$table->dropForeign('mascota_vacuna_idvacuna_foreign');
		});
		Schema::table('historial', function(Blueprint $table) {
			$table->dropForeign('historial_idmascota_foreign');
		});
	}
}