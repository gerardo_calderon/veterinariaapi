<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMascotaVacunaTable extends Migration {

	public function up()
	{
		Schema::create('mascota_vacuna', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->date('fecha');
			$table->date('fecha_proxima');
			$table->float('dosis', 8,2);
			$table->integer('estado');
			$table->integer('idmascota')->unsigned();
			$table->integer('idvacuna')->unsigned();
			$table->string('descripcion', 255);
		});
	}

	public function down()
	{
		Schema::drop('mascota_vacuna');
	}
}