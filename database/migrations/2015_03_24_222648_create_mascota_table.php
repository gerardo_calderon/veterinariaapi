<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMascotaTable extends Migration {

	public function up()
	{
		Schema::create('mascota', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('nombre', 45);
			$table->date('fecha_nacimiento');
			$table->string('icono', 255);
			$table->integer('idusuario')->unsigned();
			$table->integer('idtipo')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('mascota');
	}
}