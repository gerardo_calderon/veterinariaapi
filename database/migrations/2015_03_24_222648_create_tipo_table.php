<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoTable extends Migration {

	public function up()
	{
		Schema::create('tipo', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('nombre', 45);
			$table->string('raza', 45);
		});
	}

	public function down()
	{
		Schema::drop('tipo');
	}
}