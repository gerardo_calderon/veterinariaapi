<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuarioTable extends Migration {

	public function up()
	{
		Schema::create('usuario', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('nombre', 45);
			$table->string('correo', 100)->unique();
			$table->string('password', 100);
			$table->string('token', 100);
		});
	}

	public function down()
	{
		Schema::drop('usuario');
	}
}