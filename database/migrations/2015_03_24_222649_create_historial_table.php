<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHistorialTable extends Migration {

	public function up()
	{
		Schema::create('historial', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->date('fecha');
			$table->float('altura', 8,2);
			$table->float('peso', 8,2);
			$table->integer('idmascota')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('historial');
	}
}